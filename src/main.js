import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

Vue.config.productionTip = false

new Vue({
  router,
  watch: {
    '$route':{
      handler: (to, from) => {
        document.title = to.meta.title === 'Главная' ? 'Клуб ЦСХ' : to.meta.title + ' | Клуб ЦСХ'
      },
      immediate: true
    }
  },
  render: (h) => h(App),
}).$mount("#app");
