import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Gallery from './views/Gallery.vue'
import Drawing from './views/Drawing.vue'
import Interview from './views/Interview.vue'
import Project from './views/Project.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      meta: {
        title: 'Главная'
      },
      component: Home
    },
    {
      path: '/gallery',
      name: 'gallery',
      meta: {
        title: 'Галерея'
      },
      component: Gallery
    },
    {
      path: '/drawing',
      name: 'drawing',
      meta: {
        title: 'Чертежи'
      },
      component: Drawing
    },
    {
      path: '/interview',
      name: 'interview',
      meta: {
        title: 'Интервью'
      },
      component: Interview
    },
    {
      path: '/project',
      name: 'project',
      meta: {
        title: 'Проект'
      },
      component: Project
    }
  ]
})
